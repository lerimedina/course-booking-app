import {useState, useEffect, useContext} from 'react';
import{Form, Button} from 'react-bootstrap';
import UserContext from "../UserContext";
import {Navigate, useNavigate} from "react-router-dom"; 
import Swal from 'sweetalert2';

export default function Register(){

	const [email, setEmail]= useState("");
	const [firstName, setFirstName]= useState("");
	const [lastName, setLastName]= useState("");
	const [mobile, setMobile]= useState("");
	const [password1, setPassword1]= useState("");
	const [password2, setPassword2]= useState("");
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false)

	const { user, setUser } = useContext(UserContext)

	const navigate = useNavigate();
	
	// Check if values are successfully binded
	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	// Function to simulate user registration
	function registerUser(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},
			body : JSON.stringify({
				email: email

			})
		})
		.then(res=>res.json())
		.then(data=>{
			if(data){
				Swal.fire({
				  icon: 'error',
				  title: 'Duplicate email found',
				  text: 'Please provide a different email'
				})
			}else{
				fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
					method : "POST",
					headers : {
						"Content-Type" : "application/json"
					},
					body : JSON.stringify({
						email: email,
						firstName : firstName,
						lastName : lastName,
						mobileNo : mobile,
						password : password1

					})
				})
				.then(res=>res.json())
				.then(data=>{
					Swal.fire({
					  icon: 'success',
					  title: 'Registration successful',
					  text: 'Welcome to Zuitt!'
					})
				navigate("/login")
				})
			}	
		})

		// Clear input fields
		setEmail("");
		setPassword1("");
		setPassword2("");
		setLastName("");
		setFirstName("");
		setMobile("");

	}

	useEffect(()=>{
		// Validation to enable submit button when all fields are populated and both passwords match
		if((email!==""&&password1!==""&&password2!==""&&firstName!==""&&lastName!==""&&mobile!=="")&&(password1===password2)&&(mobile.length>=11)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password1, password2])

	return(
		(user.id)?
			<Navigate to="/courses"/>
			:
			<Form onSubmit={(e)=>registerUser(e)}>
				<Form.Group className = "mt-3"  controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter First Name"
						value={firstName}
						onChange={e=>setFirstName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group className = "mt-3" controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter Last Name"
						value={lastName}
						onChange={e=>setLastName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group className = "mt-3"  controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter Email"
						value={email}
						onChange={e=>setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">We'll never share your email with anyone</Form.Text>
				</Form.Group>

				<Form.Group className = "mt-3" controlId="mobile">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter Mobile Number"
						value={mobile}
						onChange={e=>setMobile(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group className = "mt-3"  controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter a password"
						value={password1}
						onChange={e=>setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group className = "mt-3"  controlId="password2">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Confirm your password"
						value={password2}
						onChange={e=>setPassword2(e.target.value)}
						required
					/>
				</Form.Group>
				{/*Conditional rendering for submit button based on isActive state*/}
				{
					isActive ? 
					<Button variant="danger" type="submit" id="submitBtn" className="mt-2">Submit</Button>
					:
					<Button variant="danger" type="submit" id="submitBtn" className="mt-2" disabled>Submit</Button>

				}
			</Form>
	)
}