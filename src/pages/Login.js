import {useState, useEffect, useContext} from 'react';
import{Form, Button} from 'react-bootstrap';
// import usersData from '../data/usersData';
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom"; 
import Swal from 'sweetalert2'

export default function Login(){
	// Allows us to consume the User context object and it's properties to use for user validation
	const { user, setUser } = useContext(UserContext)
	const [email, setEmail]= useState("");
	const [password, setPassword]= useState("");
	const [isActive, setIsActive] = useState(false)

	
	function loginUser(e){

		e.preventDefault();

		// Process a fetch request to the corresponding backend API
		// The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
		// The fetch request will communicate with our backend application providing it with a stringified JSON
		// Convert the information retrieved from the backend into a JavaScript object using the ".then(res => res.json())"
		// Capture the converted data and using the ".then(data => {})"
		// Syntax
		    // fetch('url', {options})
		    // .then(res => res.json())
		    // .then(data => {})
		fetch("http://localhost:4000/users/login",{
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},
			body : JSON.stringify({
				email : email,
				password : password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			if(typeof data.access !== "undefined"){
				// If no user information is found, the "access" property will not be available and will return undefined
		        // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
				localStorage.setItem('token', data.access);
				console.log(data.access);
				retrieveUserDetails(data.access)
				Swal.fire({
				  icon: 'success',
				  title: 'Login successful',
				  text: 'Welcome to Zuitt!'
				})
			}else{
				Swal.fire({
				  icon: 'error',
				  title: 'Authentication failed',
				  text: 'Please check your login credentials and try again'
				})
			}
		})

		// Set the email of the authenticated user in the local storage
        // Syntax
        	// localStorage.setItem('propertyName', value);
		// localStorage.setItem("email", email);
		// Set the global user state to have properties obtained from local storage
        // Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
        // When states change components are rerendered and the AppNavbar component will be updated based on the user credentials, unlike when using the localStorage where the localStorage does not trigger component rerendering
		/*setUser({
			email: localStorage.getItem('email')
		})*/
		/*
		- the "localStorage.setItem" allows us to manipulate the browser's localStorage property to store information indefinitely to help demonstrate conditional rendering and the login and logout features.
					- Because React JS is a single page application, using the localStorage does not trigger rerendering of components and for us to be able to view the effects of this we would need to refresh our browser.
		*/
		setEmail("");
		setPassword("");
	}

	const retrieveUserDetails = (token) => {
		fetch("http://localhost:4000/users/details",{
			headers : {
				"Authorization" : `Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			setUser({
				id:data._id,
				isAdmin	: data.isAdmin
			})
		})
	}

	useEffect(()=>{
		if(email!==""&&password!==""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password])

	return(
		(user.id)?
			<Navigate to="/courses"/>
			:
			<Form onSubmit={(e)=>loginUser(e)}>
				<h2>Login</h2>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter Email"
						value={email}
						onChange={e=>setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter a password"
						value={password}
						onChange={e=>setPassword(e.target.value)}
						required
					/>
				</Form.Group>
				{
					isActive ? 
					<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Login</Button>
					:
					<Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>Login</Button>

				}
			</Form>
	)
}