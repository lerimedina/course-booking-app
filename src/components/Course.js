import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({course}) {
    // Checks to see if the data was successfully passed
    console.log(course);
    // Every component receives information in a form of an object
    console.log(typeof course);

    const {name, description, price, _id} = course;
    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Syntax
        // const [getter, setter] = useState(initialGetterValue);

    /*const [count, setCount] = useState(0);
    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element
    console.log(useState(0))

    const [seats, setSeats] = useState(30);*/

    /*function enroll(){
        if(seats>0){
            setSeats(seats-1);
            setCount(count+1);
        }
        // }else{
        //     alert("No more seats");
        // }
    }
*/
    /*useEffect(()=>{
        if(seats===0){
            alert("no more seats")
        }
    },[seats])*/



    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {price}</Card.Text>
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}
